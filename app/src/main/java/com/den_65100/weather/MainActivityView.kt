package com.den_65100.weather

import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface MainActivityView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showResult(result: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showNegativeMessage(errorMessage: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPositiveMessage()
}