package com.den_65100.weather

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import org.json.JSONObject
import java.net.URL

@InjectViewState
class MainActivityPresenter(context: Context) : MvpPresenter<MainActivityView>(), LocationListener {

    private val disposables = CompositeDisposable()
    private var locationManager: LocationManager? = null
    private var latitude = 0.0
    private var longitude = 0.0


    @SuppressLint("MissingPermission")
    fun weatherDisplayByLocation(context: Context) {
        locationManager =
            getSystemService(context, LOCATION_SERVICE::class.java) as LocationManager?
        locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 5f, this)
        locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 5f, this)
        if (locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) == true) {
            val locationGps = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (locationGps != null) {
                this.latitude = locationGps.latitude
                this.longitude = locationGps.longitude
            }
        }
        if (locationManager?.isProviderEnabled(LocationManager.NETWORK_PROVIDER) == true) {
            val locationNet =
                locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            if (locationNet != null) {
                this.latitude = locationNet.latitude
                this.longitude = locationNet.longitude
            }
        }
        val key = "4d7b7c3ebc7563eacf3bb7ad9bb6bad8"
        val url = "https://api.openweathermap.org/data/2.5/weather?lat=$latitude" +
                "&lon=$longitude&appid=$key&units=metric&lang=ru"
        val disposable = Completable.fromAction {
            val apiResponse = URL(url).readText()
            Log.d("INFO", apiResponse)
            val weather = JSONObject(apiResponse).getJSONArray("weather")
            val desc = weather.getJSONObject(0).getString("description")
            val main = JSONObject(apiResponse).getJSONObject("main")
            val temp = main.getString("temp")
            val result = "Температура: $temp\n$desc"
            viewState.showResult(result)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { viewState.showPositiveMessage() },
                { error ->
                    viewState.showNegativeMessage("$error")
                    error.printStackTrace()
                }
            )
        disposables.add(disposable)
    }

    override fun onLocationChanged(location: Location) {
    }

    fun weatherDisplayByCity(city: String) {
        val key = "4d7b7c3ebc7563eacf3bb7ad9bb6bad8"
        val url = "https://api.openweathermap.org/data/2.5/weather?q=$city" +
                "&appid=$key&units=metric&lang=ru"

        val disposable = Completable.fromAction {
            val apiResponse = URL(url).readText()
            Log.d("INFO", apiResponse)
            val weather = JSONObject(apiResponse).getJSONArray("weather")
            val desc = weather.getJSONObject(0).getString("description")
            val main = JSONObject(apiResponse).getJSONObject("main")
            val temp = main.getString("temp")
            val result = "Температура: $temp\n$desc"
            viewState.showResult(result)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { viewState.showPositiveMessage() },
                { error ->
                    viewState.showNegativeMessage("$error")
                    error.printStackTrace()
                }
            )
        disposables.add(disposable)
    }
}
