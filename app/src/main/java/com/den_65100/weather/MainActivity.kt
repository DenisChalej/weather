package com.den_65100.weather

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import com.den_65100.weather.databinding.ActivityMainBinding
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter


class MainActivity : MvpAppCompatActivity(), MainActivityView {

    @InjectPresenter
    lateinit var presenter: MainActivityPresenter
    lateinit var binding: ActivityMainBinding
    private var interAd: InterstitialAd? = null


    @ProvidePresenter
    fun providePresenter(): MainActivityPresenter {

        return MainActivityPresenter(applicationContext)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initAdMob()
        (application as AppMainState).showAdIfAvailable(this) {
        }

        binding.mainBtn.setOnClickListener {
            if (binding.userField.text?.toString()?.trim()?.equals("")!!)
                presenter.weatherDisplayByLocation(this)
            else {
                val city = binding.userField.text.toString()
                presenter.weatherDisplayByCity(city)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        binding.adView.resume()
        loadInterAd()

    }

    override fun onPause() {
        super.onPause()
        binding.adView.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.adView.destroy()
    }

    private fun initAdMob() {
        MobileAds.initialize(this)
        val adRequest = AdRequest.Builder().build()
        binding.adView.loadAd(adRequest)
    }

    fun loadInterAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this,
            "ca-app-pub-3940256099942544/1033173712", adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(p0: LoadAdError) {
                    interAd = null
                }

                override fun onAdLoaded(ad: InterstitialAd) {
                    interAd = ad
                }
            })
    }

    private fun showInterAd() {
        if (interAd != null) {
            interAd?.fullScreenContentCallback =
                object : FullScreenContentCallback() {
                    override fun onAdDismissedFullScreenContent() {
                        showContent()
                        interAd = null
                        loadInterAd()
                    }

                    override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                        showContent()
                        interAd = null
                        loadInterAd()
                    }

                    override fun onAdShowedFullScreenContent() {
                        interAd = null
                        loadInterAd()
                    }
                }
            interAd?.show(this)
        } else {
            showContent()
        }
    }

    private fun showContent() {
        Toast.makeText(this, "Запуск контента", Toast.LENGTH_LONG).show()
    }

    override fun showResult(result: String) {
        binding.resultInfo.text = result
    }

    override fun showNegativeMessage(errorMessage: String) {
        Toast.makeText(this, "Error: $errorMessage", Toast.LENGTH_LONG).show()
    }

    override fun showPositiveMessage() {
        showInterAd()
    }
}